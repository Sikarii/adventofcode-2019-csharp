using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Collections.Generic;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] line1 = File
                .ReadAllText("inputs/input3_1.txt")
                .Split(",");
            
            string[] line2 = File
                .ReadAllText("inputs/input3_2.txt")
                .Split(",");
            
            List<Point> path1 = GetPath(line1);
            List<Point> path2 = GetPath(line2);

            var intersects = path1.Intersect(path2).Where(p => p.X != 0 && p.Y != 0);

            var part1 = intersects.Min(p => Math.Abs(p.X) + Math.Abs(p.Y));
            var part2 = intersects.Min(p => path1.IndexOf(p) + path2.IndexOf(p));

            Console.WriteLine("Part 1: " + part1);
            Console.WriteLine("Part 2: " + part2);
        }

        static List<Point> GetPath(string[] commands)
        {
            List<Point> path = new List<Point>
            {
                new Point(0, 0)
            };

            foreach (var command in commands)
            {
                var lastPos = path.Last();

                var direction = command[0];
                var moveAmount = int.Parse(command[1..]);
        
                int xMove = 0;
                int yMove = 0;

                switch (direction)
                {
                    case 'R':
                    {
                        ++xMove;
                        break;
                    }
                    case 'D':
                    {
                        --yMove;
                        break;
                    }
                    case 'L':
                    {
                        --xMove;
                        break;
                    }
                    case 'U':
                    {
                        ++yMove;
                        break;
                    }
                    default:
                    {
                        throw new Exception("Invalid direction given: " + direction);
                    }
                }

                for (int i = 0; i < moveAmount; i++)
                {
                    int newX = lastPos.X + xMove;
                    int newY = lastPos.Y + yMove;

                    lastPos = new Point(newX, newY);
                    path.Add(lastPos);
                }
            }

            return path;
        }
    }
}