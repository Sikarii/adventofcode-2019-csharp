﻿using System;
using System.IO;
using System.Linq;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] masses = File
                .ReadAllLines("inputs/input1.txt")
                .Select(s => int.Parse(s))
                .ToArray();

            int part1 = CalculateTotalFuel(masses);
            int part2 = CalculateTotalFuelMass(masses);

            Console.WriteLine($"Part 1: {part1}");
            Console.WriteLine($"Part 2: {part2}");
        }

        static int CalculateFuel(int value)
        {
            return (value / 3) - 2;
        }

        static int CalculateFuelMass(int value)
        {
            int fuel = CalculateFuel(value);
            if (fuel <= 0)
            {
                return 0;
            }

            return (fuel + CalculateFuelMass(fuel));
        }

        static int CalculateTotalFuel(int[] values)
        {
            return values.Sum(value => CalculateFuel(value));
        }

        static int CalculateTotalFuelMass(int[] values)
        {
            return values.Sum(value => CalculateFuelMass(value));
        }
    }
}
