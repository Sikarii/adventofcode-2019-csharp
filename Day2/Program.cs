﻿using System;
using System.IO;
using System.Linq;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = File
                .ReadAllText("inputs/input2.txt")
                .Split(',')
                .Select(l => int.Parse(l))
                .ToArray();

            numbers[1] = 12;
            numbers[2] = 2;

            int[] numbersCopy = (int[])numbers.Clone();

            int output1 = RunIntCode(numbersCopy);
            Console.WriteLine($"Part 1: {output1}");

            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    numbersCopy = (int[])numbers.Clone();

                    numbersCopy[1] = i;
                    numbersCopy[2] = j;

                    int output2 = RunIntCode(numbersCopy);
                    if (output2 != 19690720)
                    {
                        continue;
                    }

                    Console.WriteLine($"Part 2: {i} and {j}");
                    return;
                }
            }
        }

        static int RunIntCode(int[] numbers)
        {
            int cursor = 0;
            while (numbers[cursor] != 99)
            {
                int opCode = numbers[cursor];
                int addr1 = numbers[cursor + 1];
                int addr2 = numbers[cursor + 2];
                int varAddr = numbers[cursor + 3];

                int op1 = numbers[addr1];
                int op2 = numbers[addr2];

                int result = HandleOpCode(opCode, op1, op2);
                numbers[varAddr] = result;
                cursor += 4;
            }

            return numbers[0];
        }

        static int HandleOpCode(int opCode, int value1, int value2)
        {
            if (opCode == 1) return (value1 + value2);
            if (opCode == 2) return (value1 * value2);

            throw new ArgumentException("Invalid opcode passed: " + opCode);
        }
    }
}