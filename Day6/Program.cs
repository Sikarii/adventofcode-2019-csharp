﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Day6
{
    public class Planet
    {
        public string Name { get; }

        public Planet Parent { get; set; }

        public int DirectOrbitCount
        {
            get
            {
                return Parent == null ? 0 : 1;
            }
        }

        public int IndirectOrbitCount
        {
            get
            {
                // Orbiting nothing...
                if (Parent == null)
                {
                    return 0;
                }
 
                return Parent.DirectOrbitCount + Parent.IndirectOrbitCount;
            }
        }

        public Planet(string name) => Name = name;
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] mapData = File.ReadAllLines("inputs/input6.txt");

            List<Planet> planets = ParseMap(mapData);

            var directOrbitCount = planets.Sum(p => p.DirectOrbitCount);
            var indirectOrbitCount = planets.Sum(p => p.IndirectOrbitCount);
            var totalOrbitsCount = (directOrbitCount + indirectOrbitCount);

            var you = planets.Where(p => p.Name == "YOU").First();
            var san = planets.Where(p => p.Name == "SAN").First();

            var youPath = GetShortestPathToCOM(you);
            var sanPath = GetShortestPathToCOM(san);

            // If we find the first common planet, the rest of the route is the same
            var firstCommon = youPath.Intersect(sanPath).First();

            var youCountToCommon = youPath.IndexOf(firstCommon);
            var sanCountToCommon = sanPath.IndexOf(firstCommon);
            var totalTransferCount = (youCountToCommon + sanCountToCommon);

            Console.WriteLine("Part 1: " + totalOrbitsCount);
            Console.WriteLine("Part 2: " + totalTransferCount);
        }

        static List<Planet> GetShortestPathToCOM(Planet planet)
        {
            var shortestPath = new List<Planet>();

            // Only COM doesnt have a parent!
            while (planet.Parent != null)
            {
                planet = planet.Parent;
                shortestPath.Add(planet);
            }

            return shortestPath;
        }

        static List<Planet> ParseMap(string[] mapData)
        {
            var output = new List<Planet>();

            foreach (var data in mapData)
            {
                var planetData = data.Split(")");

                var parentName = planetData[0];
                var childName = planetData[1];

                Planet parent = output.Where(p => p.Name == parentName).FirstOrDefault();
                if (parent == null)
                {
                    parent = new Planet(parentName);
                    output.Add(parent);
                }

                Planet child = output.Where(p => p.Name == childName).FirstOrDefault();
                if (child == null)
                {
                    child = new Planet(childName);
                    output.Add(child);
                }

                child.Parent = parent;
            }

            return output;
        }
    }
}