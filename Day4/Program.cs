using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] range = File
                .ReadAllText("inputs/input4.txt")
                .Split("-")
                .Select(r => int.Parse(r))
                .ToArray();

            int lowerRange = range[0];
            int upperRange = range[1];

            List<string> Candicates = Enumerable
                .Range(lowerRange, (upperRange - lowerRange))
                .Select(c => c.ToString())
                .ToList();

            List<string> part1 = Candicates.Where(c => HasValidPasswordCriteria(c, r => r.Count() >= 2)).ToList();
            List<string> part2 = Candicates.Where(c => HasValidPasswordCriteria(c, r => r.Count() == 2)).ToList();

            Console.WriteLine("Part 1: " + part1.Count);
            Console.WriteLine("Part 2: " + part2.Count);
        }

        static bool HasValidPasswordCriteria(string candicate, Func<IGrouping<char, char>, bool> adjacentDigitsCondition)
        {
            if (string.IsNullOrWhiteSpace(candicate))
            {
                return false;
            }

            if (candicate.Length != 6)
            {
                return false;
            }

            /*
                "123455":
                    [0]: '1'
                    [1]: '2'
                    [2]: '3'
                    [3]: '4'
                    [4]: '5'
                    [5]: '5'
                    -> SequenceEqual: TRUE
            */
            /*
                "121567":
                    [0]: '1'
                    [1]: '1'
                    [2]: '2'
                    [3]: '5'
                    [4]: '6'
                    [5]: '7'
                    -> SequenceEqual: FALSE
            */
            bool increasingDigits = candicate.OrderBy(c => c).SequenceEqual(candicate);
            if (!increasingDigits)
            {
                return false;
            }

            /*
                "123555":
                    [0]: '1'
                    [1]: '2'
                    [2]: '3'
                    [3]: '5', '5', '5'
                    -> Adjacent digits with 3 grouping,
                    will not pass part 2 because no group of 2 exists
            */
            /*
                "111667":
                    [0]: '1', '1', '1'
                    [1]: '6', '6'
                    [2]: '7'
                    -> Adjacent digits with 3 grouping,
                    but still passes part 2 because a group of 2 exists
            */
            return candicate.GroupBy(c => c).Any(adjacentDigitsCondition);
        }
    }
}